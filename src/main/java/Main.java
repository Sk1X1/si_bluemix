import com.ibm.cloud.sdk.core.service.security.IamOptions;
import com.ibm.watson.visual_recognition.v3.VisualRecognition;
import com.ibm.watson.visual_recognition.v3.model.*;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Main class which can train model, classify images or delete existing classifiers.
 *
 * @author Jiri PLACEK, Hana SVOBODOVA
 */
public class Main {
    private final static String PATH = "H:\\OneDrive\\ZCU\\SI\\Integrační projekt\\imgs\\";
    private final static String API_KEY = "-";
    private final static String VERSION_DATE = "2019-04-19";
    private final static String CLASSIFIER_ID = "DefaultCustomModel_133003818";

    private static VisualRecognition service;

    /**
     * Main method. In default state, it will initialize VisualRecognition and print status
     * of classifier defined by ID in constant.
     *
     * @param args Program arguments
     */
    public static void main(String[] args){
        initalize();
        /*try {
            //train();
            //update(CLASSIFIER_ID);
            //classify(CLASSIFIER_ID, PATH + "test\\1.jpg", "1.jpg");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }*/
        checkStatus(CLASSIFIER_ID);
    }

    /**
     * Deletes classifier.
     *
     * @param classifierId Id of classifier that should be deleted
     */
    private static void deleteClassifier(String classifierId) {
        DeleteClassifierOptions options = new DeleteClassifierOptions.Builder()
            .classifierId(classifierId)
            .build();
        service.deleteClassifier(options).execute().getResult();
    }

    /**
     * Check and print status of given classifier.
     */
    private static void checkStatus(String classifierId) {
        GetClassifierOptions getClassifierOptions = new GetClassifierOptions.Builder()
                .classifierId(classifierId)
                .build();
        Classifier classifier = service.getClassifier(getClassifierOptions).execute().getResult();
        System.out.println("Status: " + classifier.getStatus());
    }

    /**
     * Initialize VisualRecognition.
     */
    private static void initalize() {
        service = new VisualRecognition(VERSION_DATE);
        IamOptions iamOptions = new IamOptions.Builder()
                .apiKey(API_KEY)
                .build();
        service.setIamCredentials(iamOptions);
    }

    /**
     * Classify given image.
     *
     * @param classifierId Id of used classifier
     * @param imagePath Path of classified image (including file name)
     * @param imageName File name of classified image
     * @throws FileNotFoundException
     */
    private static void classify(String classifierId, String imagePath, String imageName) throws FileNotFoundException {
        ClassifyOptions options = new ClassifyOptions.Builder()
                .imagesFile(new File(imagePath))
                .imagesFilename(imageName)
                .addClassifierId(classifierId)
                .build();
        ClassifiedImages result = service.classify(options).execute().getResult();
        System.out.println(result);
    }

    /**
     * Trains new classifier
     *
     * @return Id of new classifier
     * @throws FileNotFoundException
     */
    private static String train() throws FileNotFoundException {
        CreateClassifierOptions createOptions = new CreateClassifierOptions.Builder()
                .name("Castles")
                .addPositiveExamples("Karlstejn", new File(PATH + "karlstejn.zip"))
                .addPositiveExamples("Svihov", new File(PATH + "svihov.zip"))
                .addPositiveExamples("Loket", new File(PATH + "loket.zip"))
                .negativeExamples(new File(PATH + "_none.zip"))
                .build();
        Classifier classifier = service.createClassifier(createOptions).execute().getResult();
        System.out.println(classifier);
        return classifier.getClassifierId();
    }

    /**
     * Updates classifier.
     *
     * @param classifierId Id of updated classifier
     * @throws FileNotFoundException
     */
    private static void update(String classifierId) throws FileNotFoundException {
        UpdateClassifierOptions updateOptions = new UpdateClassifierOptions.Builder()
                .classifierId(classifierId)
                .addPositiveExamples("Krivoklat", new File(PATH + "krivoklat.zip"))
                .build();
        Classifier updatedFoo = service.updateClassifier(updateOptions).execute().getResult();
        System.out.println(updatedFoo);
    }
}
